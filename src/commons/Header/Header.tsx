import React, { ReactNode } from "react";
import NavigationBar from "./NavigationBar";
import styles from "./header.module.scss";
import HeroBanner from "./HeroBanner";

type Props = {};

const Header = () => {
  return (
    <header className={styles.header}>
      <NavigationBar />
      <HeroBanner
        title={"Create and share your photo stories."}
        description={
          "Photosnap is a platform for photographers and visual storytellers. We make it easy to share photos, tell stories and connect with others."
        }
        buttonLabel={"GET AN INVITE"}
      />
    </header>
  );
};

export default Header;
