"use client";
import Image from "next/image";
import Link from "next/link";
import React, { useState } from "react";
import Button from "../Button/Button";
import styles from "./header.module.scss";
import Logo from "@/components/Logo";

type NavigationItem = {
  label: string;
  path: string;
};

const NavigationBar = () => {
  const [isMobileMenuActive, setIsMobileMenuActive] = useState(false);
  const navigationItems: Array<NavigationItem> = [
    { label: "stories", path: "/stories" },
    { label: "features", path: "/features" },
    { label: "pricing", path: "/pricing" },
  ];
  return (
      <nav className={styles.header_navigation}>
        <section className={styles.header_navigation__logo}>
          <Logo />
          <section
            onClick={() => setIsMobileMenuActive(!isMobileMenuActive)}
            className={styles.header_navigation__mobile_menu}
          >
            {isMobileMenuActive ? (
              <Image
                src="/assets/shared/mobile/close.svg"
                alt="Close button"
                width={16}
                height={15}
              />
            ) : (
              <Image
                src="/assets/shared/mobile/menu.svg"
                alt="hamburger button"
                width={20}
                height={6}
              />
            )}
          </section>
        </section>

        {isMobileMenuActive && (
          <section className={styles.header_navigation__mobile_content}>
            <ul>
              {navigationItems.map((item: NavigationItem, index: number) => (
                <li key={index} className={styles.header_navigation__link}>
                  <Link href={item.path}>{item.label}</Link>
                </li>
              ))}
            </ul>
            <hr />
            <Button type="button" label="get an invite" />
          </section>
        )}

        <section className={styles.header_navigation__links}>
          <ul>
            {navigationItems.map((item: NavigationItem, index: number) => (
              <li key={index} className={styles.header_navigation__link}>
                <Link href={item.path}>{item.label}</Link>
              </li>
            ))}
          </ul>
        </section>
        <section className={styles.header_navigation__button}>
          <Button type="button" label="get an invite" />
        </section>
      </nav>
  );
};

export default NavigationBar;
