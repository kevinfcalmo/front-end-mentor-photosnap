import { HeroBannerType } from "@/types/herobannerType";
import React from "react";
import styles from "./header.module.scss";
import ButtonWithSVG from "../Button/buttonWithSVG";

const HeroBanner = ({ title, description, buttonLabel }: HeroBannerType) => {
  return (
    <section className={styles.header_herobanner}>
      <section className={styles.header_herobanner__picture}></section>
      <section className={styles.header_herobanner__content}>
        <section
          className={styles.header_herobanner__content_rectangle}
        ></section>
        <section className={styles.header_herobanner__contents}>
          <section className={styles.header_herobanner__contents_title}>
            <h1>{title}</h1>
          </section>
          <section className={styles.header_herobanner__contents_description}>
            <p>{description}</p>
          </section>
          <section className={styles.header_herobanner__contents_button}>
            <ButtonWithSVG isWhite={true} type="button" label={buttonLabel} />
          </section>
        </section>
      </section>
    </section>
  );
};

export default HeroBanner;
