import { ButtonType } from "@/types/buttonType";
import React from "react";

const Button = ({ label, type }: ButtonType) => {
  return (
    <button className="square_btn" type={type}>
      {label}
    </button>
  );
};

export default Button;
