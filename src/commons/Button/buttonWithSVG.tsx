import { ButtonType } from "@/types/buttonType";
import Image from "next/image";
import React from "react";

const ButtonWithSVG = ({ label, type, isWhite }: ButtonType) => {
  return (
    <button type={type}>
      {label}
      <Image
        src={
          isWhite
            ? "/assets/shared/desktop/arrow_white.svg"
            : "/assets/shared/desktop/arrow.svg"
        }
        alt="arrow"
        width={43}
        height={14}
      />
    </button>
  );
};

export default ButtonWithSVG;
