import Logo from "@/components/Logo";
import styles from "./footer.module.scss";
import ButtonWithSVG from "../Button/buttonWithSVG";
import Link from "next/link";
import Image from "next/image";

type FooterSocial = {
  label: string;
};

type FooterLink = {
  label: string;
  path: string;
};

const Footer = () => {
  const footer_socials: Array<FooterSocial> = [
    { label: "facebook" },
    { label: "youtube" },
    { label: "twitter" },
    { label: "pinterest" },
    { label: "instagram" },
  ];

  const footer_links: Array<FooterLink> = [
    {
      label: "home",
      path: "/",
    },
    {
      label: "stories",
      path: "/stories",
    },
    {
      label: "features",
      path: "/features",
    },
    {
      label: "pricing",
      path: "/pricing",
    },
  ];
  return (
    <footer className={styles.footer}>
      <section className={styles.footer_logo}>
        <Logo isBlackBG={true} />
      </section>
      <section className={styles.footer_socials}>
        <ul>
          {footer_socials.map((link: FooterSocial, index: number) => (
            <li key={index}>
              <Link href="#">
                <Image
                  src={`/assets/shared/desktop/${link.label}.svg`}
                  alt={""}
                  width={20}
                  height={20}
                />
              </Link>
            </li>
          ))}
        </ul>
      </section>
      <section className={styles.footer_links}>
        <ul>
          {footer_links.map((link: FooterLink, index: number) => (
            <li key={index}>
              <Link href={link.path}>{link.label}</Link>
            </li>
          ))}
        </ul>
      </section>
      <section className={styles.footer_button}>
        <ButtonWithSVG label="get an invite" type="button" isWhite={true} />
      </section>
      <section className={styles.footer_copyright}>
        <p>Copyright 2019. All Rights Reserved</p>
      </section>
    </footer>
  );
};

export default Footer;
