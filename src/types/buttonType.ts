export type ButtonType = {
  label: string;
  type: "submit" | "reset" | "button" | undefined;
  isWhite?: boolean;
};
