export type HeroBannerType = {
  title: string;
  description: string;
  buttonLabel: string;
};
