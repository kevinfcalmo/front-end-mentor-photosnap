import Footer from "@/commons/Footer";
import Header from "@/commons/Header/Header";
import SEOComponent from "@/commons/SEO";
import ListIcones from "@/components/ListIcones";
import ListImages from "@/components/ListImages";
import SectionWithImage from "@/components/SectionWithImage";
import data from "@/data/images-list.json";
import dataShared from "@/data/shared-list.json";

export default function Home() {
  const items = data.slice(0, 4);
  return (
    <>
      <SEOComponent title="Home" />
      <Header />
      <main>
        <SectionWithImage />
        <ListImages items={items} />
        <ListIcones items={dataShared.slice(0, 3)} />
      </main>

      <Footer />
    </>
  );
}
