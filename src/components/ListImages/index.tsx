"use client";
import React, { useEffect, useState } from "react";
import styles from "./style.module.scss";
import ButtonWithSVG from "@/commons/Button/buttonWithSVG";
import Image from "next/image";
import useScreenSize from "@/hooks/useScreenSize";

type ImageType = {
  title: string;
  author: string;
  picture: string;
};

type ScreenTuple = "mobile" | "tablet" | "desktop";

const ListImages = ({ items }: { items: Array<ImageType> }) => {
  const screenSize = useScreenSize();
  const [hidden, setHidden] = useState(true);
  const [hoveredIndex, setHoveredIndex] = useState<number | null>(null);
  const [screenMode, setScreenMode] = useState<ScreenTuple | null>(null);
  const path = "/assets/stories/";
  useEffect(() => {
    return () => {
      setScreenMode("mobile");
      if (screenSize.width > 768) {
        setScreenMode("desktop");
      }
    };
  }, [screenSize.width]);
  return (
    <section className={styles.list_images}>
      {items.map((item: ImageType, index: number) => (
        <section
          onMouseEnter={() => {
            setHidden(false);
            setHoveredIndex(index);
          }}
          onMouseLeave={() => setHidden(true)}
          key={index}
          className={styles.list_images__item}
        >
          <section className={styles.list_images__item_cover}>
            <img src={path + screenMode + item.picture} alt="" />
            <section className={styles.list_images__item_overlay}>
              <section className={styles.list_images__item_content}>
                <section>
                  <h3>{item.title}</h3>
                </section>
                <section>
                  <p>by {item.author}</p>
                </section>
                <hr />
                <section className={styles.list_images__content_button}>
                  <ButtonWithSVG
                    isWhite={true}
                    type="button"
                    label="read story"
                  />
                </section>
              </section>
            </section>
            {!hidden && hoveredIndex === index && (
              <section className={styles.hover}></section>
            )}
          </section>
        </section>
      ))}
    </section>
  );
};

export default ListImages;
