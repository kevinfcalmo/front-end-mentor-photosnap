"use client";
import ButtonWithSVG from "@/commons/Button/buttonWithSVG";
import useScreenSize from "@/hooks/useScreenSize";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import SectionContent from "./SectionContent";
import styles from "./styles.module.scss";

type DataType = {
  picture: string;
  title: string;
  description: string;
  buttonLabel: string;
};

type ScreenTuple = "mobile" | "tablet" | "desktop";

const SectionWithImage = () => {
  const screenSize = useScreenSize();
  const [screenMode, setScreenMode] = useState<ScreenTuple | null>(null);
  const path = "/assets/home/";
  useEffect(() => {
    return () => {
      setScreenMode("mobile");
      if (screenSize.width > 768) {
        setScreenMode("tablet");
      }
      if (screenSize.width > 1440) {
        setScreenMode("desktop");
      }
    };
  }, [screenSize.width]);

  const data: Array<DataType> = [
    {
      picture: "/beautiful-stories.jpg",
      title: "beautiful stories every time",
      description:
        "We provide design templates to ensure your stories look terrific. Easily add photos, text, embed maps and media from other networks. Then share your story with everyone.",
      buttonLabel: "view the stories",
    },
    {
      picture: "/designed-for-everyone.jpg",
      title: "designed for everyone",
      description:
        "Photosnap can help you create stories that resonate with your audience.  Our tool is designed for photographers of all levels, brands, businesses you name it.",
      buttonLabel: "view the stories",
    },
  ];
  return (
    <section className={styles.section_main}>
      {data.map((item: DataType, index: number) => (
        <SectionContent
          key={index}
          item={item}
          path={path}
          index={index}
          screenMode={screenMode!}
        />
      ))}
    </section>
  );
};

export default SectionWithImage;
