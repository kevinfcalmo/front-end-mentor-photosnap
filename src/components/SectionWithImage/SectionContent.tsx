import ButtonWithSVG from "@/commons/Button/buttonWithSVG";
import styles from "./styles.module.scss";

type SectionContentType = {
  item: {
    picture: string;
    title: string;
    description: string;
    buttonLabel: string;
  };
  path: string;
  index: number;
  screenMode: string;
};

const SectionContent = ({
  item,
  path,
  screenMode,
  index,
}: SectionContentType) => {
  return (
    <section
      className={
        index % 2 !== 0
          ? styles.section_main__item_reverse
          : styles.section_main__item
      }
    >
      <section className={styles.section_main__picture}>
        <img src={path + screenMode + item.picture} alt="" />
      </section>
      <section className={`${styles.section_main__content}`}>
        <section className={`${styles.section_main__content_title}`}>
          <h2>{item.title}</h2>
        </section>
        <section className={styles.section_main__content_description}>
          <p>{item.description}</p>
        </section>
        <section className={styles.section_main__content_button}>
          <ButtonWithSVG type="button" label={item.buttonLabel} />
        </section>
      </section>
    </section>
  );
};

export default SectionContent;
