import React from "react";
import styles from "./styles.module.scss";
import Image from "next/image";

const ListIcones = ({ items }: any) => {
  return (
    <section className={styles.list_icones}>
      {items.map((item: any, index: number) => (
        <section key={index} className={styles.list_icones__item}>
          <Image src={item.picture} alt={item.title} width={72} height={72} />
          <h3>{item.title}</h3>
          <p>{item.description}</p>
        </section>
      ))}
    </section>
  );
};

export default ListIcones;
