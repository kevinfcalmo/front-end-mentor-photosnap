import Image from "next/image";

type LogoType = {
  isBlackBG?: boolean;
};

const Logo = ({ isBlackBG }: LogoType) => {
  return (
    <Image
      src={
        isBlackBG ? "/logo_white.svg" : "/assets/shared/desktop/logo.svg"
      }
      alt="photosnap"
      width={170}
      height={16}
    />
  );
};

export default Logo;
